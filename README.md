# Shipment Manager

Welcome to the Shipment Manager!

**This is a demo application which enables the user to view and manage shipments.**

# Contents

-   [Business need](#business-need)
-   [Use cases](#use-cases)
-   [Technology used](#technology-used)
-   [How to run](#how-to-run)

# Business need

The main goal is for the user to check the shipments at a glance. This allows users to take faster decisions and plan ahead of time.
Providing information to the customer increases transparency and reduces communication issues.

# Use cases

-   The user shall be able to:
    -   See shipments in pages of 20 elements per page
    -   Search by shipment id, name etc.
    -   Sort by different fields (e.g. id, name) in ascending/descending order
    -   View the shipment information on a separate shipment details page
    -   Update the shipment name (should persist when the page is reloaded)

The interactions should not refresh the page.

## Technology used

**React** and **JavaScript ES6+** are used as the language of choice. For styling **SCSS** is used as a pre-processor.

Apart from this, several helper libraries, such as **json-server**, **prop-types**, **font-awesome** and **husky** are utilized.

For unit testing **jest** & **enzyme** are used.

Along with all of the above **prettier**, **eslint**, **styleline** are also used to maintain coding standards.

# How to run

-   Clone this repository.
-   A RESTful API for `shipments` is provided with the repo.
-   There are scripts to start both the server and the client.
-   Use NPM or Yarn to execute scripts.
-   Run `yarn` to install all project dependencies.
-   Run `yarn start` to start the REST server and the client app.
